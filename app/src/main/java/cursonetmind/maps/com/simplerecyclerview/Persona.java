package cursonetmind.maps.com.simplerecyclerview;

/**
 * Created by A8Alumno on 21/10/2017.
 */

public class Persona {
    private String nombre;
    private int edad;
    private String aficion;
    private int image;

    public Persona(String nombre, int edad, String aficion, int image) {
        this.nombre = nombre;
        this.edad = edad;
        this.aficion = aficion;
        this.image = image;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getAficion() {
        return aficion;
    }

    public int getImage() {
        return image;
    }
}
