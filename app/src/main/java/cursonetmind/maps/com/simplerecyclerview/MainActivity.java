package cursonetmind.maps.com.simplerecyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //1 Traernos la referencia.
        RecyclerView recyclerPersona = (RecyclerView) findViewById(R.id.recyclerPersona);
        // 2 CONSTRUIR UN ADAPTER
        //2.1 PREVIO:CONSTRUCCION DE DATOS.
        ArrayList<Persona> listPersonas = dummyPersonas();
        AdapterPersonas adapter = new AdapterPersonas(this,listPersonas);
        recyclerPersona.setHasFixedSize(true);
        //3 Gestion del layoutManager
        recyclerPersona.setLayoutManager(new LinearLayoutManager(this));
        recyclerPersona.setAdapter(adapter);

    }








    private ArrayList<Persona> dummyPersonas() {
        ArrayList<Persona> listDummyPersonas = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
           listDummyPersonas.add(new Persona("pepe de "+i,24,"aficion",R.drawable.ic_account_circle_red_500_24dp));
        }
        return listDummyPersonas;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
