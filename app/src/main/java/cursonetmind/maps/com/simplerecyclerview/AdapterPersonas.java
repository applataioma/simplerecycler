package cursonetmind.maps.com.simplerecyclerview;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by A8Alumno on 21/10/2017.
 */

class AdapterPersonas extends RecyclerView.Adapter<AdapterPersonas.ViewHolder> {


    private final ArrayList<Persona> items;
    private final Activity act;

    public AdapterPersonas(Activity act, ArrayList<Persona> listPersonas) {
        this.items = listPersonas;
        this.act = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_persona, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(String.format(act.getString(R.string.name), items.get(position).getNombre()));
        holder.age.setText(String.valueOf(items.get(position).getEdad()));
        holder.img.setImageResource(items.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView age;
        private ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_layout_persona_name);
            age = itemView.findViewById(R.id.item_layout_persona_edad);
            img = itemView.findViewById(R.id.item_layout_persona_img);
        }
    }
}
